﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TvMazeScraper.Lib.DataAccess.Entities;
using TvMazeScraper.Lib.Domain;
using TvMazeScraper.Lib.Domain.Repositories;

namespace TvMazeScraper.Lib.DataAccess.Repositories
{
    internal class TvShowRepository : ITvShowRepository
    {
        private readonly DataContext _dbContext;

        public TvShowRepository(DataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddAsync(TvShow show)
        {
            var current = await _dbContext
                .TvShows
                .Include(s => s.Actors)
                .ThenInclude(sa => sa.Actor)
                .SingleOrDefaultAsync(s => s.Id == show.Id);

            if (current != null)
                await RemoveAsync(current);

            var entity = await Convert(show);
            await _dbContext.AddAsync(entity);
            await _dbContext.SaveChangesAsync();
        }

        private async Task RemoveAsync(TvShowEntity current)
        {
            _dbContext.Remove(current);
            foreach (var links in current.Actors)
            {
                _dbContext.Remove(links);
            }

            await _dbContext.SaveChangesAsync();
            //DetachAll();
        }

        //public void DetachAll()
        //{
        //    var entityEntries = _dbContext.ChangeTracker.Entries().ToArray();

        //    foreach (var entityEntry in entityEntries)
        //    {
        //        entityEntry.State = EntityState.Detached;
        //    }
        //}

        private async Task<TvShowEntity> Convert(TvShow show)
        {
            var showActors = new List<TvShowActorsEntity>();

            foreach (var actor in show.Actors)
            {
                var actorEntity = await _dbContext.Actors.SingleOrDefaultAsync(a => a.Id == actor.Id);
                showActors.Add(new TvShowActorsEntity
                {
                    Actor = actorEntity ?? new ActorEntity()
                    {
                        Id = actor.Id,
                        Name = actor.Name,
                        Birthday = actor.Birthday
                    }
                });
            }

            return new TvShowEntity
            {
                Id = show.Id,
                Name = show.Name,
                Actors = showActors
            };
        }

        public async Task<int?> GetHighestShowId()
        {
            try
            {
               return await _dbContext.TvShows.MaxAsync(s => s.Id);
            } 
            catch (InvalidOperationException)
            {
                return (int?)null;
            }
        }

        public async Task<IEnumerable<TvShow>> GetShowList(int page, int pageSize)
        {
            var skip = pageSize * page;
            var result = await _dbContext
                .TvShows
                .Include(s => s.Actors)
                .ThenInclude(sa => sa.Actor)
                .Skip(skip)
                .Take(pageSize)
                .ToListAsync();

            return result.Select(s => new TvShow
            {
                Id = s.Id,
                Name = s.Name,
                Actors = s.Actors.OrderByDescending(a => a.Actor.Birthday).Select(a => new Actor()
                {
                    Id = a.Actor.Id,
                    Name = a.Actor.Name,
                    Birthday = a.Actor.Birthday
                }).ToList()
            });
        }
    }
}
