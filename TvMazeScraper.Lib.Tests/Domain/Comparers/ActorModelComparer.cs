﻿using System;
using System.Collections.Generic;
using TvMazeScraper.Lib.Domain.Models;

namespace TvMazeScraper.Lib.Tests.Domain.Comparers
{
    public class ActorModelComparer : IEqualityComparer<ActorModel>
    {
        public bool Equals(ActorModel x, ActorModel y)
        {
            if (x != null && y == null)
                return false;
            if (x == null && y != null)
                return false;
            if (ReferenceEquals(x, y))
                return true;

            return x.Person.Id == y.Person.Id && x.Person.Name == y.Person.Name && x.Person.Birthday == y.Person.Birthday;
        }

        public int GetHashCode(ActorModel obj) => HashCode.Combine(obj.Person.Id, obj.Person.Name, obj.Person.Birthday);
    }
}
