# RTL TvMaze Scraper

Deze repository bevat de uitwerking van opdracht van de RTL TvMaze Scraper ten behoeve van een freelance positie.

## Project layout

Het project is opgedeeld in een 4-tal projecten:
- TvMazeAPI
	- Bevat de REST Api
- TvMazeScraper.Lib
	- Bevat zowel de Domain layer als de DataAccess layer. In grotere projecten zou ik deze over twee losse assemblies splitsen maar gezien de compactheid van het project heb ik het bij 1 assembly gehouden. Wel netjes onderverdeeld in mapjes.
- TvMazeScraper.Lib.Tests
	- De unit testen voor de diverse onderdelen van het project. Niet alles is voorzien van een unit test en niet alles is volledig afgetest vanwege tijdbeperkingen. Project gevat geen integratie testen.
- TvMazeScraper.Scraper
	- Losse applicatie die gedraaid kan worden om de TvMaze API te scrapen.
	
## Niet behaalde doelen

Bij aanvang van de bouw had ik ook de volgende doelen gesteld. Door tijdsgebrek heb ik deze uiteindelijk niet uitgevoerd:
- Docker container voor de Scraper.
- Docker-compose file om volledige project met 1 commando te kunnen draaien, bestaande uit:
	- SQL Server
	- Scraper
	- WebAPI

## Project starten

Om het project te draaien is een MS SQL Server nodig. Indien niet beschikbaar kan deze in een Docker container worden gedraaid door het volgende commando uit te voeren:
	`docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=Pa$$w0rdSQL' -e "MSSQL_PID=Express" -p 1500:1433 --name sqltestcontainer -d mcr.microsoft.com/mssql/server:2019-latest`

De connectionstrings in de config file zijn ingesteld op het bovenstaande scenario.

De database kan worden aangemaakt door in Visual Studio de `Package Manager Console` te openen, het `TvMazeScraper.Lib` project als default te selecteren, `TvMazeAPI` *of* `TvMazeScraper.Scraper` als startup project te selecteren en `update-database` in de package manager console te draaien. Eventueel kan de connection string ook via een user-secret worden gezet. Dit kan door in een powershell/command prompt in het TvMazeScraper.Lib project `dotnet user-secrets set "ConnectionStrings:DefaultConnection" "<ConnectionStringGoesHere>"` uit te voeren. 

Hierna kunnen zowel de Scraper als de WebAPI worden gestart. 
