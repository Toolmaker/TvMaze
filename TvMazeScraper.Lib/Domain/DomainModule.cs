﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Polly;
using Polly.Extensions.Http;
using System;
using System.Net.Http;
using TvMazeScraper.Lib.Configuration;
using TvMazeScraper.Lib.Domain.Clients;

namespace TvMazeScraper.Lib.Domain
{
    public static class DomainModule
    {
        public static void Configure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<TvShowScraper>();
            services.AddTransient<ITvMazeClient, TvMazeClient>();
            services.AddTransient<IConfigProvider, ConfigProvider>();

            services.AddHttpClient<ITvMazeClient, TvMazeClient>(client =>
            {
                client.BaseAddress = new Uri(configuration["Urls:BaseAddress"]);
            })
                .AddPolicyHandler(GetRetryPolicy());
        }

        static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .OrResult(msg => msg.StatusCode == System.Net.HttpStatusCode.TooManyRequests)
                .WaitAndRetryForeverAsync((retryAttempt) => TimeSpan.FromSeconds(20));
        }
    }
}
