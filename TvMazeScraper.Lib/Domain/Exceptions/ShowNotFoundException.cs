﻿using System;

namespace TvMazeScraper.Lib.Domain.Exceptions
{
    public class ShowNotFoundException : Exception
    {
        public ShowNotFoundException(int showId)
            : base($"Show with Id {showId} does not exist")
        { 
        }
    }
}
