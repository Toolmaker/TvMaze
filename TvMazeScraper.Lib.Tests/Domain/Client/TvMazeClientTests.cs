﻿using Bogus;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using TvMazeScraper.Lib.Domain.Clients;
using TvMazeScraper.Lib.Domain.Exceptions;
using TvMazeScraper.Lib.Domain.Models;
using TvMazeScraper.Lib.Tests.Domain.Comparers;
using Xunit;

namespace TvMazeScraper.Lib.Tests.Domain.Client
{
    public class TvMazeClientTests
    {
        private static readonly Faker Faker = new Faker();

        private readonly TvMazeClientFixture _fixture = new TvMazeClientFixture();
        private TvMazeClient CreateSUT() => new TvMazeClient(_fixture.CreateClient(), _fixture.Config);

        [Fact]
        public async Task GetShowsAsync_WithNegativePageNumber_ThrowsArgumentOutOfRangeException()
        {
            var pageNumber = Faker.Random.Int(Int32.MinValue, -1);

            var sut = CreateSUT();
            await Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => sut.GetShowsAsync(pageNumber, CancellationToken.None));
        }

        [Fact]
        public async Task GetShowsAsync_WithValidPageNumber_ReturnsItems()
        {
            var pageNumber = Faker.Random.Int(0, 1024);
            var expectedResult = _fixture.GenerateTvShows(2);

            var uri = string.Format(_fixture.Config.ShowListUri, pageNumber);
            _fixture.SetupGetRequest(uri, expectedResult);

            var sut = CreateSUT();
            var result = await sut.GetShowsAsync(pageNumber, CancellationToken.None);

            Assert.Equal(expectedResult, result, new TvShowModelComparer());
        }



        [Fact]
        public async Task GetShowsAsync_WithNonExistingPage_ReturnsEmptyList()
        {
            var pageNumber = Faker.Random.Int(0, 1024);
            var uri = string.Format(_fixture.Config.ShowListUri, pageNumber);
            _fixture.SetupGetRequest(uri, HttpStatusCode.NotFound);

            var sut = CreateSUT();
            var result = await sut.GetShowsAsync(pageNumber, CancellationToken.None);

            Assert.Empty(result);
        }

        [Fact]
        public async Task GetCastList_WithInvalidArguments_ThrowsArgumentException()
        {
            var showId = Faker.Random.Int(Int32.MinValue, 0);
            var sut = CreateSUT();
            await Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => sut.GetCastListAsync(showId, CancellationToken.None));
        }

        [Fact]
        public async Task GetCastList_WithInvalidShowId_ThrowsShowNotFoundException()
        {
            var showId = Faker.Random.Int(0);

            var uri = string.Format(_fixture.Config.CastListUri, showId);
            _fixture.SetupGetRequest($"shows/{showId}/cast", HttpStatusCode.NotFound);

            var sut = CreateSUT();
            await Assert.ThrowsAsync<ShowNotFoundException>(() => sut.GetCastListAsync(showId, CancellationToken.None));
        }

        [Fact]
        public async Task GetCastList_WithValidShowId_ReturnsCast()
        {
            var showId = Faker.Random.Int(0);
            var expectedResult = _fixture.GenerateCast(Faker.Random.Int(10, 20));

            var uri = string.Format(_fixture.Config.CastListUri, showId);
            _fixture.SetupGetRequest(uri, expectedResult);

            var sut = CreateSUT();
            var result = await sut.GetCastListAsync(showId, CancellationToken.None);

            Assert.Equal(expectedResult, result, new ActorModelComparer());
        }

        // This is a nasty bug actually found in TvMaze.
        //[Fact]
        //public async Task GetCastAsync_WithDuplicateEntries_Deduplicates()
        //{
        //    var showId = Faker.Random.Int(0);
        //    var expectedResult = _fixture.GenerateCast(1);
        //    expectedResult.Add(new ActorModel()
        //    {
        //        Person = new ActorModel.PersonModel() 
        //        {
        //            Id = expectedResult[0].Person.Id,
        //            Name = expectedResult[0].Person.Name,
        //            Birthday = expectedResult[0].Person.Birthday,
        //        }
        //    });

        //    var uri = string.Format(_fixture.Config.CastListUri, showId);
        //    _fixture.SetupGetRequest(uri, expectedResult);

        //    var sut = CreateSUT();
        //    var result = await sut.GetCastListAsync(showId, CancellationToken.None);

        //    Assert.Equal(expectedResult.Take(1), result, new ActorModelComparer());
        //}
    }
}

