﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TvMazeScraper.Lib.Domain.Repositories
{
    public interface ITvShowRepository
    {
        Task AddAsync(TvShow show);
        Task<int?> GetHighestShowId();
        Task<IEnumerable<TvShow>> GetShowList(int page, int pageSize);
    }
}
