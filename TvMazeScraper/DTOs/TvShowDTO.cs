﻿using System.Collections.Generic;

namespace TvMazeScraper.API.DTOs
{
    public class TvShowDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<ActorDTO> Cast {get; set;}
    }
}
