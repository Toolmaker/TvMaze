﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Polly;
using System;
using System.Threading;
using System.Threading.Tasks;
using TvMazeScraper.Lib.Domain;

namespace TvMazeScraper.Scraper
{
    internal class ScraperHostedService : IHostedService
    {
        private readonly ILogger<ScraperHostedService> _logger;
        private readonly IServiceProvider _serviceProvider;
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private Timer _timer;
        private bool _isRunning = false;
        
        public ScraperHostedService(ILogger<ScraperHostedService> logger, IServiceProvider serviceProvider)
        {
            _logger = logger;
            _serviceProvider = serviceProvider;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromHours(24));

            await Task.CompletedTask;
        }

        private void DoWork(object _)
        {
            try
            {
                _isRunning = true;

                var scraper = (TvShowScraper)_serviceProvider.GetService(typeof(TvShowScraper));
                var policy = Policy
                    .Handle<Exception>(ex => !(ex is OperationCanceledException))
                    .RetryForeverAsync();

                policy.ExecuteAsync(async () => await scraper.ScrapeAsync(_cancellationTokenSource.Token))                
                    .GetAwaiter()
                    .GetResult();
            }
            catch (OperationCanceledException)
            {
                _isRunning = false;
            }
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _cancellationTokenSource.Cancel();

            while (_isRunning)
                await Task.Delay(10);
        }
    }
}
