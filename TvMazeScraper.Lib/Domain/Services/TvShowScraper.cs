﻿using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TvMazeScraper.Lib.Domain.Clients;
using TvMazeScraper.Lib.Domain.Repositories;

namespace TvMazeScraper.Lib.Domain
{
    public class TvShowScraper
    {
        private const int PageSize = 250;

        private readonly ITvMazeClient _client;
        private readonly ILogger<TvShowScraper> _logger;
        private ITvShowRepository _repository;
        
        private bool _running = true;
        
        public TvShowScraper(ITvMazeClient client, ITvShowRepository repository, ILogger<TvShowScraper> logger)
        {
            _client = client;
            _repository = repository;
            _logger = logger;
        }

        public async Task ScrapeAsync(CancellationToken token)
        {
            var page = await GetNextPageAsync();

            while (_running)
            {
                _logger.LogInformation($"Scraping page {page}");
                var shows = await _client.GetShowsAsync(page, token);
                foreach (var show in shows)
                {
                    _logger.LogInformation($"Scraping show with id {show.Id}");

                    var cast = await _client.GetCastListAsync(show.Id, token);

                    // De-deplicate the cast, this actually happens on the second show, might be in other shows as well.
                    // Caused me a severe headache while writing this thinking I did something wrong with EFCore.
                    cast = cast
                        .GroupBy(c => c.Person.Id)
                        .SelectMany(g => g.Take(1))
                        .ToList();

                    var tvShow = new TvShow
                    {
                        Id = show.Id,
                        Name = show.Name,
                        Actors = cast.Select(a => new Actor { Id = a.Person.Id, Name = a.Person.Name, Birthday = a.Person.Birthday }).ToList()
                    };

                    await _repository.AddAsync(tvShow);
                }
                page++;
            }
        }

        private async Task<int> GetNextPageAsync()
        {
            var highestId = await _repository.GetHighestShowId();
            var result = (highestId ?? 0) / 250.0;
            return (int)Math.Floor(result);
        }
    }
}
