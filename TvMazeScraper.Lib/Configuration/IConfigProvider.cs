﻿namespace TvMazeScraper.Lib.Configuration
{
    public interface IConfigProvider
    {
        string ConnectionString { get; }
        string BaseAddress { get; }
        string ShowListUri { get; }
        string CastListUri { get; }
    }
}
