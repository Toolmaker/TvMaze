﻿using System;
using System.Collections.Generic;

namespace TvMazeScraper.Lib.Domain
{ 
    public class Actor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? Birthday { get; set; }
        public IList<TvShow> TvShows { get; set; }
    }
}
