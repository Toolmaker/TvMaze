﻿using System;
using System.Collections.Generic;
using TvMazeScraper.Lib.Domain.Models;

namespace TvMazeScraper.Lib.Tests.Domain.Comparers
{
    public class TvShowModelComparer : IEqualityComparer<TvShowModel>
    {
        public bool Equals(TvShowModel x, TvShowModel y)
        {
            if (x == null && y != null)
                return false;
            if (x != null && y == null)
                return false;
            if (ReferenceEquals(x, y))
                return true;

            return x.Id == x.Id && y.Name == y.Name;
        }

        public int GetHashCode(TvShowModel obj) => HashCode.Combine(obj.Id, obj.Name);
    }
}
