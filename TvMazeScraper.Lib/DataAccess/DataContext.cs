﻿using Microsoft.EntityFrameworkCore;
using TvMazeScraper.Lib.DataAccess.Extensions;
using TvMazeScraper.Lib.DataAccess.Entities;

namespace TvMazeScraper.Lib.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<TvShowEntity> TvShows { get; set; }
        public DbSet<ActorEntity> Actors { get; set; }
        public DbSet<TvShowActorsEntity> TvShowsActors { get; set; }

        public DataContext(DbContextOptions<DataContext> options) 
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyAllConfigurations();
        }
    }
}
