﻿namespace TvMazeScraper.Lib.DataAccess.Entities
{
    public class TvShowActorsEntity
    {
        public int ShowId { get; set; }
        public TvShowEntity Show { get; set; }
        public int ActorId { get; set; }
        public ActorEntity Actor { get; set; }
    }
}
