﻿using Microsoft.Extensions.Configuration;

namespace TvMazeScraper.Lib.Configuration
{
    public class ConfigProvider : IConfigProvider
    {
        private readonly IConfiguration _configuration;

        public ConfigProvider(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string ConnectionString => _configuration["ConnectionStrings:DefaultConnection"];

        public string BaseAddress => _configuration["Urls:BaseAddress"];

        public string ShowListUri => _configuration["Urls:ShowList"];

        public string CastListUri => _configuration["Urls:CastList"];
    }
}
