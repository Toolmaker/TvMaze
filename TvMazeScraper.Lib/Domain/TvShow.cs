﻿using System.Collections.Generic;

namespace TvMazeScraper.Lib.Domain
{ 
    public class TvShow
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IList<Actor> Actors { get; set; }
    }
}
