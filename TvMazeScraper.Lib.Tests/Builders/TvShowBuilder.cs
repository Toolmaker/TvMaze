﻿using Bogus;
using TvMazeScraper.Lib.Domain.Models;

namespace TvMazeScraper.Lib.Tests.Builders
{
    internal class TvShowBuilder : Faker<TvShowModel>
    {
        public TvShowBuilder()
        {
            RuleFor(s => s.Id, f => f.Random.Int());
            RuleFor(s => s.Name, f => f.Lorem.Lines(1));
        }
    }
}
