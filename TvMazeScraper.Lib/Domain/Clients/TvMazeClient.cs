﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TvMazeScraper.Lib.Configuration;
using TvMazeScraper.Lib.Domain.Exceptions;
using TvMazeScraper.Lib.Domain.Models;

namespace TvMazeScraper.Lib.Domain.Clients
{
    public interface ITvMazeClient
    {
        Task<IEnumerable<ActorModel>> GetCastListAsync(int showId, CancellationToken token);
        Task<IEnumerable<TvShowModel>> GetShowsAsync(int page, CancellationToken token);
    }

    public class TvMazeClient : ITvMazeClient
    {
        private readonly HttpClient _client;
        private readonly IConfigProvider _configurationProvider;

        public TvMazeClient(HttpClient httpClient, IConfigProvider configurationProvider)
        {
            _client = httpClient;
            _configurationProvider = configurationProvider;
        }

        public async Task<IEnumerable<TvShowModel>> GetShowsAsync(int page, CancellationToken cancellationToken)
        {
            if (page < 0)
                throw new ArgumentOutOfRangeException(nameof(page), -1, "Only positive page numbers are allowed.");

            var uri = string.Format(_configurationProvider.ShowListUri, page);
            var result = await _client.GetAsync(uri, cancellationToken);
            if (result.IsSuccessStatusCode)
                return await DeserializeResult<List<TvShowModel>>(result);

            if (result.StatusCode == HttpStatusCode.NotFound)
                return Array.Empty<TvShowModel>();

            throw new InvalidOperationException("Unhandled HTTP status code was returned by the server.");
        }

        private async Task<T> DeserializeResult<T>(HttpResponseMessage result)
        {
            var content = await result.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(content);
        }

        public async Task<IEnumerable<ActorModel>> GetCastListAsync(int showId, CancellationToken cancellationToken)
        {
            if (showId < 0)
                throw new ArgumentOutOfRangeException(nameof(showId), showId, "Only positive page numbers are allowed.");

            var uri = string.Format(_configurationProvider.CastListUri, showId);
            var result = await _client.GetAsync(uri, cancellationToken);
            if (result.IsSuccessStatusCode)
                return await DeserializeResult<IEnumerable<ActorModel>>(result);

            if (result.StatusCode == HttpStatusCode.NotFound)
                throw new ShowNotFoundException(showId);

            throw new InvalidOperationException("Unhandled HTTP status code was returned by the server.");
        }
    }
}
