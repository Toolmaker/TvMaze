﻿using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TvMazeScraper.Lib.Domain;
using TvMazeScraper.Lib.Domain.Clients;
using TvMazeScraper.Lib.Domain.Models;
using TvMazeScraper.Lib.Domain.Repositories;
using TvMazeScraper.Lib.Tests.Builders;
using TvMazeScraper.Lib.Tests.Domain.Comparers;
using Xunit;

namespace TvMazeScraper.Lib.Tests.Domain
{
    public class TvShowScraperTests
    {
        private readonly Mock<ITvMazeClient> _client = new Mock<ITvMazeClient>();
        private readonly Mock<ITvShowRepository> _repository = new Mock<ITvShowRepository>();
        private readonly Mock<ILogger<TvShowScraper>> _logger = new Mock<ILogger<TvShowScraper>>();

        private static readonly TvShowBuilder TvShowBuilder = new TvShowBuilder();
        private static readonly CastBuilder CastBuilder = new CastBuilder();

        private TvShowScraper CreateSUT() => new TvShowScraper(_client.Object, _repository.Object, _logger.Object);
        private CancellationTokenSource _cancellationSource = new CancellationTokenSource();

        [Fact]
        public async Task ScrapeAsync_WithoutPriorRun_StartsScrapingAtShow1()
        {
            var shows = TvShowBuilder.Generate(10);
            var cast = CastBuilder.Generate(12);

            // TODO: Write a mapper for this?
            var expectedResult = new TvShow()
            {
                Id = shows[0].Id,
                Name = shows[0].Name,
                Actors = cast.Select(a => new Actor() { Id = a.Person.Id, Name = a.Person.Name, Birthday = a.Person.Birthday }).ToList()
            };
            TvShow actualResult = null;

            _repository
                .Setup(r => r.GetHighestShowId())
                .Returns(Task.FromResult((int?)null));

            _client
                .Setup(c => c.GetShowsAsync(0, It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult((IEnumerable<TvShowModel>)shows));

            _client.Setup(c => c.GetCastListAsync(shows[0].Id, It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult((IEnumerable<ActorModel>)cast));

            _repository
                .Setup(r => r.AddAsync(It.IsAny<TvShow>()))
                .Callback<TvShow>((show) =>
                {
                    actualResult = show;
                    
                    // Since none of the mocks actually checks the CancellationToken we'll use an Exception here.
                    throw new Exception("Cancelling further execution");
                });
                
            var sut = CreateSUT();
            await RunAndSwallowException(async () => await sut.ScrapeAsync(_cancellationSource.Token));

            Assert.Equal(expectedResult, actualResult, new TvShowComparer());
        }

        private async Task RunAndSwallowException(Func<Task> action)
        {
            try 
            {
                await action();
            } 
            catch (Exception) 
            {
                // Empty on purpose
            }
        }
    }
}
