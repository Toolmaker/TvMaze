﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using TvMazeScraper.API.DTOs;
using TvMazeScraper.Lib.Domain.Repositories;

namespace TvMazeScraper.API.Controllers
{
    [ApiController]
    [Route("[shows]")]
    public class TvShowController : Controller
    {
        /// <summary>
        /// Gets a paginated list of shows
        /// </summary> 
        /// <response code="200">Request succeeded successfully.</response>
        /// <response code="400">page or pageSize parameters are invalid.</response>   
        /// <response code="404">No shows found on the requested page.</response>   
        [HttpGet("/")]
        public async Task<IActionResult> GetShowList([FromServices] ITvShowRepository repository, int page = 0, int pageSize = 100)
        {
            if (page < 0 || pageSize < 0)
                return BadRequest("page and pageSize should be positive integers.");

            var shows = await repository.GetShowList(page, pageSize);
            if (!shows.Any())
                return NotFound();

            var result = shows.Select(s => new TvShowDTO()
            {
                Id = s.Id,
                Name = s.Name,
                Cast = s.Actors.Select(a => new ActorDTO
                {
                    Id = a.Id,
                    Name = a.Name,
                    Birthday = a.Birthday
                }).ToList()
            });

            return Ok(result);
        }
    }
}
