﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TvMazeScraper.Lib.DataAccess.Entities;

namespace TvMazeScraper.Lib.DataAccess.Configurations
{
    internal class TvShowEntityTypeConfiguration : IEntityTypeConfiguration<TvShowEntity>
    {
        public void Configure(EntityTypeBuilder<TvShowEntity> builder)
        {
            builder.ToTable("TvShows");

            builder
                .HasKey(s => s.Id);

            builder
                .Property(s => s.Id).ValueGeneratedNever();

            builder.HasMany(s => s.Actors);
        }
    }
}
