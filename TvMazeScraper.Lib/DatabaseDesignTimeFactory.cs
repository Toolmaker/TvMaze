﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using TvMazeScraper.Lib.DataAccess;

namespace TvMazeScraper.Lib
{
    public class DatabaseDesignTimeFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .AddUserSecrets<DatabaseDesignTimeFactory>()
                .Build();

            var connectionString = configuration["ConnectionStrings:DefaultConnection"];
            //System.Console.WriteLine(connectionString);
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseSqlServer(connectionString)
                .Options;

            return new DataContext(options);
        }
    }
}
