﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TvMazeScraper.Lib.DataAccess.Entities;

namespace TvMazeScraper.Lib.DataAccess.Configurations
{
    public class TvShowActorsEntityTypeConfiguration : IEntityTypeConfiguration<TvShowActorsEntity>
    {
        public void Configure(EntityTypeBuilder<TvShowActorsEntity> builder)
        {
            builder.ToTable("TvShowsActors");

            builder.HasKey(sa => new { sa.ShowId, sa.ActorId });

            builder
                .HasOne(sa => sa.Show)
                .WithMany(s => s.Actors)
                .HasForeignKey(sa => sa.ShowId);

            builder
                .HasOne(sa => sa.Actor)
                .WithMany(a => a.TvShows)
                .HasForeignKey(sa => sa.ActorId);
        }
    }
}
