﻿using System.Collections.Generic;

namespace TvMazeScraper.Lib.DataAccess.Entities
{
    public class TvShowEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<TvShowActorsEntity> Actors { get; set; }
    }
}
