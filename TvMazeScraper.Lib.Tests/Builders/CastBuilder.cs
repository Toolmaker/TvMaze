﻿using Bogus;
using TvMazeScraper.Lib.Domain.Models;
using static TvMazeScraper.Lib.Domain.Models.ActorModel;

namespace TvMazeScraper.Lib.Tests.Builders
{
    internal class CastBuilder : Faker<ActorModel>
    {
        public CastBuilder()
        {
            RuleFor(a => a.Person, f => new PersonModel()
            {
                Id = f.Random.Int(0),
                Name = f.Person.FullName,
                Birthday = f.Person.DateOfBirth
            });
        }
    }
}
