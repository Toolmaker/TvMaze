﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TvMazeScraper.Lib.DataAccess.Repositories;
using TvMazeScraper.Lib.Domain.Repositories;

namespace TvMazeScraper.Lib.DataAccess
{
    public static class DataAccessModule
    {
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration["ConnectionStrings:DefaultConnection"];
            services.AddDbContext<DataContext>(opts => opts.UseSqlServer(connectionString), ServiceLifetime.Transient);

            services.AddTransient<ITvShowRepository, TvShowRepository>();
        }
    }
}
