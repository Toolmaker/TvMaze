﻿using System;

namespace TvMazeScraper.Lib.Domain.Models
{
    public class ActorModel
    {
        public PersonModel Person { get; set; }

        public class PersonModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public DateTime? Birthday { get; set; }
        }
    }
}
