﻿using System;
using System.Collections.Generic;
using TvMazeScraper.Lib.Domain;

namespace TvMazeScraper.Lib.Tests.Domain.Comparers
{
    internal class ActorComparer : IEqualityComparer<Actor>
    {
        public bool Equals(Actor x, Actor y)
        {
            if (x != null && y == null)
                return false;
            if (x == null && y != null)
                return false;
            if (ReferenceEquals(x, y))
                return true;

            return x.Id == y.Id && x.Name == y.Name && x.Birthday == y.Birthday;
        }

        public int GetHashCode(Actor obj) => HashCode.Combine(obj.Id, obj.Name, obj.Birthday);
    }
}
