﻿using System;
using System.Collections.Generic;

namespace TvMazeScraper.Lib.DataAccess.Entities
{
    public class ActorEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? Birthday { get; set; }
        public ICollection<TvShowActorsEntity> TvShows { get; set; }
    }
}
