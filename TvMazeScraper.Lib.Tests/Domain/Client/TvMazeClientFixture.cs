﻿using Moq;
using Moq.Contrib.HttpClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using TvMazeScraper.Lib.Configuration;
using TvMazeScraper.Lib.Domain.Models;
using TvMazeScraper.Lib.Tests.Builders;

namespace TvMazeScraper.Lib.Tests.Domain.Client
{
    internal class TvMazeClientFixture
    {
        private readonly Mock<HttpMessageHandler> _handler = new Mock<HttpMessageHandler>();
        private static readonly TvShowBuilder TvShowBuilder = new TvShowBuilder();
        private static readonly CastBuilder CastBuilder = new CastBuilder();

        public IConfigProvider Config { get; } = new ConfigMock().Object;

        public HttpClient CreateClient()
        {
            var client = _handler.CreateClient();
            client.BaseAddress = new Uri(Config.BaseAddress);

            return client;
        }

        public void SetupGetRequest<T>(string uri, T response)
        {
            _handler
                .SetupRequest(HttpMethod.Get, new Uri($"{Config.BaseAddress}{uri}"))
                .ReturnsResponse(JsonConvert.SerializeObject(response), "application/json");
        }

        public void SetupGetRequest(string uri, HttpStatusCode statusCode)
        {
            _handler
                .SetupRequest(HttpMethod.Get, new Uri($"{Config.BaseAddress}{uri}"))
                .ReturnsResponse(statusCode);
        }

        public List<TvShowModel> GenerateTvShows(int count) => TvShowBuilder.Generate(count);

        public List<ActorModel> GenerateCast(int count) => CastBuilder.Generate(count);
    }
}
