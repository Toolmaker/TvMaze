﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Threading.Tasks;
using TvMazeScraper.Lib.DataAccess;
using TvMazeScraper.Lib.Domain;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace TvMazeScraper.Scraper
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var builder = new HostBuilder()
                .ConfigureServices((hostContext, services) =>
                {
                    DataAccessModule.Configure(services, hostContext.Configuration);
                    DomainModule.Configure(services, hostContext.Configuration);

                    services.AddHostedService<ScraperHostedService>();
                })
                .ConfigureLogging((context, logBuilder) =>
                {
                    logBuilder.AddConsole();
                })
                .ConfigureAppConfiguration((context, configBuilder) =>
                {
                    configBuilder
                        //.AddUserSecrets<Program>()
                        //.AddEnvironmentVariables()
                        .AddJsonFile("appsettings.json");
                });

            await builder.RunConsoleAsync();
        }
    }
}
