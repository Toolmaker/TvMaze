﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TvMazeScraper.Lib.DataAccess.Entities;

namespace TvMazeScraper.Lib.DataAccess.Configurations
{
    public class ActorEntityTypeConfiguration : IEntityTypeConfiguration<ActorEntity>
    {
        public void Configure(EntityTypeBuilder<ActorEntity> builder)
        {
            builder.ToTable("Actors");

            builder.HasKey(a => a.Id);
            builder.Property(a => a.Id).ValueGeneratedNever();
            builder.HasMany(a => a.TvShows);
        }
    }
}
