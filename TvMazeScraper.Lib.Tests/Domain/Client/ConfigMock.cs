﻿using Moq;
using TvMazeScraper.Lib.Configuration;

namespace TvMazeScraper.Lib.Tests.Domain.Client
{
    public class ConfigMock : Mock<IConfigProvider>
    {
        public ConfigMock()
        {
            Setup(p => p.BaseAddress).Returns("http://api.tvmaze.com/");

            Setup(p => p.ShowListUri).Returns("shows?page={0}");
            Setup(p => p.CastListUri).Returns("shows/{0}/cast");
        }
    }
}
