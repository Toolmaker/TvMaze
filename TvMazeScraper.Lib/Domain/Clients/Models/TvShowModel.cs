﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TvMazeScraper.Lib.Domain.Models
{
    public class TvShowModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
