﻿using System;
using System.Collections.Generic;
using System.Linq;
using TvMazeScraper.Lib.Domain;

namespace TvMazeScraper.Lib.Tests.Domain.Comparers
{
    public class TvShowComparer : IEqualityComparer<TvShow>
    {
        public bool Equals(TvShow x, TvShow y)
        {
            if (x == null && y != null)
                return false;
            if (x != null && y == null)
                return false;
            if (ReferenceEquals(x, y))
                return true;

            var actorComparer = new ActorComparer();
            var actorsEqual = x.Actors.All(a => y.Actors.Any(b => actorComparer.Equals(a, b)));
            return x.Id == x.Id && y.Name == y.Name && actorsEqual;
        }

        public int GetHashCode(TvShow obj) => HashCode.Combine(obj.Id, obj.Name);
    }
}
